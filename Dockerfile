# Stage 1: Install dependencies and run tests
FROM node:20 AS builder
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run test
RUN npm run build

# Stage 2: Prepare the distribution
FROM node:20 AS dist
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/dist ./dist
