import KeystrokeTrackerSDK from '../index';

global.fetch = jest.fn(() =>
  Promise.resolve({
    ok: true,
    json: () => Promise.resolve({ message: 'success' }),
  })
) as jest.Mock;

describe('KeystrokeTrackerSDK', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should send keystroke event data to the configured API endpoint', async () => {
    const sdk = new KeystrokeTrackerSDK('https://api.example.com');
    sdk.init();

    const event = new KeyboardEvent('keydown', {
      key: 'A',
    });
    document.dispatchEvent(event);

    expect(global.fetch).toHaveBeenCalledTimes(1);
    expect(global.fetch).toHaveBeenCalledWith(
      'https://api.example.com',
      expect.objectContaining({
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: expect.stringContaining('"key":"A"'),
      })
    );
  });
});
