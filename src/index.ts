type SDKConfig = string;

class KeystrokeTrackerSDK {
  private apiEndpoint: string;

  constructor(apiEndpoint: SDKConfig) {
    if (!apiEndpoint) {
      throw new Error('API endpoint is required to initialize the SDK.');
    }
    this.apiEndpoint = apiEndpoint;
  }

  public init(): void {
    document.addEventListener('keydown', this.handleKeystroke);
  }

  private handleKeystroke = (event: KeyboardEvent): void => {
    const eventData = {
      key: event.key,
      timestamp: new Date().toISOString(),
    };

    this.sendEventToAPI(eventData);
  };

  private async sendEventToAPI(eventData: { key: string; timestamp: string }): Promise<void> {
    try {
      const response = await fetch(this.apiEndpoint, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(eventData),
      });

      if (!response.ok) {
        throw new Error(`Error sending event data to API: ${response.statusText}`);
      }

      console.log('Event data sent successfully to the API.');
    } catch (error) {
      console.error('Failed to send event data to API:', error);
    }
  }
}

export default KeystrokeTrackerSDK;
