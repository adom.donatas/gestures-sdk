# Gestures SDK

The Gestures SDK is a TypeScript-based software development kit designed by Donatas Adomavicius, aimed at tracking user gestures within web applications. This lightweight SDK captures keystrokes and sends them to a configurable API endpoint, making it ideal for analytics, user behavior tracking, and interactive applications.

## Features

- Tracks user keystrokes in real-time.
- Configurable API endpoint for sending tracked data.
- Easy to integrate into any web application.
- Written in TypeScript for robust application development.

## Installation

To install the Gestures SDK in your project, run:

```bash
npm install @adom.donatas/gestures-sdk
```

or if you are using yarn:

```bash
yarn add @adom.donatas/gestures-sdk
```

## Usage

To use the SDK in your project, import and initialize it with your desired API endpoint as shown below:

```javascript
import GesturesSDK from '@adom.donatas/gestures-sdk';

const gesturesSDK = new GesturesSDK('https://your-api-endpoint.com');
gesturesSDK.init();
```

## Running Tests

To run the integration tests:

```bash
npm test
```

This command executes the test suite defined with Jest, ensuring the SDK's functionality works as expected.

## Demo

Please note that the demo API and website might be in a sleeping state. Once opened, please wait for a minute or two, and the demo instances should wake up.

- **Demo API Log**: Explore the log of events recorded by the demo API [here](https://fake-node-server.onrender.com/events).

- **Demo Web App**: Access the web application designed to track gestures and send them to the demo API [here](https://gestures-demo-app.onrender.com).

Feel free to interact with the demo to explore its functionality. If you encounter any issues, please allow some time for the instances to wake up before retrying.
